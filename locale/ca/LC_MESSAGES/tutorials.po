# Translation of docs_krita_org_tutorials.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-29 19:35+0100\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../tutorials.rst:5
msgid "Tutorials and Howto's"
msgstr "Guies d'aprenentatge i Com es fa (Howto)"

#: ../../tutorials.rst:7
msgid ""
"Learn through developer and user generated tutorials to see Krita in action."
msgstr ""
"Apreneu de les guies d'aprenentatge generades pels desenvolupadors i usuaris "
"per a veure el Krita en acció."

#: ../../tutorials.rst:9
msgid "Contents:"
msgstr "Contingut:"
