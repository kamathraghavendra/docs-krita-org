# Translation of docs_krita_org_reference_manual___tools___calligraphy.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-24 16:36+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../<rst_epilog>:16
msgid ""
".. image:: images/icons/calligraphy_tool.svg\n"
"   :alt: toolcalligraphy"
msgstr ""
".. image:: images/icons/calligraphy_tool.svg\n"
"   :alt: eina de cal·ligrafia"

#: ../../reference_manual/tools/calligraphy.rst:1
msgid "Krita's calligraphy tool reference."
msgstr "Referència de l'eina Cal·ligrafia del Krita."

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Tools"
msgstr "Eines"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Vector"
msgstr "Vectors"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Path"
msgstr "Camí"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Variable Width Stroke"
msgstr "Traç d'amplada variable"

#: ../../reference_manual/tools/calligraphy.rst:11
msgid "Calligraphy"
msgstr "Cal·ligrafia"

#: ../../reference_manual/tools/calligraphy.rst:16
msgid "Calligraphy Tool"
msgstr "Eina de cal·ligrafia"

#: ../../reference_manual/tools/calligraphy.rst:18
msgid "|toolcalligraphy|"
msgstr "|toolcalligraphy|"

#: ../../reference_manual/tools/calligraphy.rst:20
msgid ""
"The Calligraphy tool allows for variable width lines, with input managed by "
"the tablet. Press down with the stylus/left mouse button on the canvas to "
"make a line, lifting the stylus/mouse button ends the stroke."
msgstr ""
"L'eina de cal·ligrafia permet línies d'amplada variable, amb l'entrada "
"gestionada per la tauleta. Premeu amb el botó del llapis/clic esquerre sobre "
"el llenç per a crear una línia, llevant el llapis/botó del ratolí per a "
"finalitzar el traç."

#: ../../reference_manual/tools/calligraphy.rst:24
msgid "Tool Options"
msgstr "Opcions de l'eina"

#: ../../reference_manual/tools/calligraphy.rst:26
msgid "**Fill**"
msgstr "**Emplenat**"

#: ../../reference_manual/tools/calligraphy.rst:28
msgid "Doesn't actually do anything."
msgstr "En realitat no fa res."

#: ../../reference_manual/tools/calligraphy.rst:30
msgid "**Calligraphy**"
msgstr "**Cal·ligrafia**"

#: ../../reference_manual/tools/calligraphy.rst:32
msgid ""
"The drop-down menu holds your saved presets, the :guilabel:`Save` button "
"next to it allows you to save presets."
msgstr ""
"El desplegable conté els valors preestablerts desats, el botó :guilabel:"
"`Desa` del costat permet desar els valors preestablerts."

#: ../../reference_manual/tools/calligraphy.rst:34
msgid "Follow Selected Path"
msgstr "Segueix el camí seleccionat"

#: ../../reference_manual/tools/calligraphy.rst:35
msgid ""
"If a stroke has been selected with the default tool, the calligraphy tool "
"will follow this path."
msgstr ""
"Si s'ha seleccionat un traç amb l'eina predeterminada, l'eina de "
"cal·ligrafia seguirà aquest camí."

#: ../../reference_manual/tools/calligraphy.rst:36
msgid "Use Tablet Pressure"
msgstr "Usa la pressió de la tauleta"

#: ../../reference_manual/tools/calligraphy.rst:37
msgid "Uses tablet pressure to control the stroke width."
msgstr "Utilitza la pressió de la tauleta per a controlar l'amplada del traç."

#: ../../reference_manual/tools/calligraphy.rst:38
msgid "Thinning"
msgstr "Aprima"

#: ../../reference_manual/tools/calligraphy.rst:39
msgid ""
"This allows you to set how much thinner a line becomes when speeding up the "
"stroke. Using a negative value makes it thicker."
msgstr ""
"Permet establir quant més prima es tornarà una línia en accelerar el traç. "
"Utilitzeu un valor negatiu per a fer-lo més gruixut."

#: ../../reference_manual/tools/calligraphy.rst:40
msgid "Width"
msgstr "Amplada"

#: ../../reference_manual/tools/calligraphy.rst:41
msgid "Base width for the stroke."
msgstr "Amplada base per al traç."

#: ../../reference_manual/tools/calligraphy.rst:42
msgid "Use Tablet Angle"
msgstr "Usa l'angle de la tauleta"

#: ../../reference_manual/tools/calligraphy.rst:43
msgid ""
"Allows you to use the tablet angle to control the stroke, only works for "
"tablets supporting it."
msgstr ""
"Permet utilitzar l'angle de la tauleta per a controlar el traç, només "
"funcionarà per a les tauletes que ho admetin."

#: ../../reference_manual/tools/calligraphy.rst:44
msgid "Angle"
msgstr "Angle"

#: ../../reference_manual/tools/calligraphy.rst:45
msgid "The angle of the dab."
msgstr "L'angle del toc."

#: ../../reference_manual/tools/calligraphy.rst:46
msgid "Fixation"
msgstr "Fixació"

#: ../../reference_manual/tools/calligraphy.rst:47
msgid "The ratio of the dab. 1 is thin, 0 is round."
msgstr "La relació del toc. 1 és prim, 0 és rodó."

#: ../../reference_manual/tools/calligraphy.rst:48
msgid "Caps"
msgstr "Majúscules"

#: ../../reference_manual/tools/calligraphy.rst:49
msgid "Whether or not an stroke will end with a rounding or flat."
msgstr "Si un traç acabarà o no amb un arrodoniment o pla."

#: ../../reference_manual/tools/calligraphy.rst:50
msgid "Mass"
msgstr "Massa"

#: ../../reference_manual/tools/calligraphy.rst:51
msgid ""
"How much weight the stroke has. With drag set to 0, high mass increases the "
"'orbit'."
msgstr ""
"Quant pes tindrà el traç. Amb Arrossega establert a 0, una massa alta farà "
"augmentar l'«òrbita»."

#: ../../reference_manual/tools/calligraphy.rst:53
msgid "Drag"
msgstr "Arrossega"

#: ../../reference_manual/tools/calligraphy.rst:53
msgid ""
"How much the stroke follows the cursor, when set to 0 the stroke will orbit "
"around the cursor path."
msgstr ""
"Quant seguirà el traç al cursor, quan s'estableix a 0, el traç orbitarà al "
"voltant del camí del cursor."

#: ../../reference_manual/tools/calligraphy.rst:57
msgid ""
"The calligraphy tool can be edited by the edit-line tool, but currently you "
"can't add or remove nodes without converting it to a normal path."
msgstr ""
"L'eina de cal·ligrafia es pot editar amb l'eina de línies d'edició, però "
"actualment no pot afegir o eliminar nodes sense convertir-los en un camí "
"normal."
