# Translation of docs_krita_org_reference_manual___dr_minw_debugger.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 16:03+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-screen.png"
msgstr ".. image:: images/Mingw-crash-screen.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-explorer-path.png"
msgstr ".. image:: images/Mingw-explorer-path.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-log-start.png"
msgstr ".. image:: images/Mingw-crash-log-start.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-log-end.png"
msgstr ".. image:: images/Mingw-crash-log-end.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-dbg7zip.png"
msgstr ".. image:: images/Mingw-dbg7zip.png"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-dbg7zip-dir.png"
msgstr ".. image:: images/Mingw-dbg7zip-dir.png"

#: ../../reference_manual/dr_minw_debugger.rst:1
msgid "How to get a backtrace in Krita using the dr. MinW debugger."
msgstr ""
"Com aconseguir una traça inversa al Krita emprant el depurador Dr. MinW."

#: ../../reference_manual/dr_minw_debugger.rst:13
msgid "Backtrace"
msgstr "Traça inversa"

#: ../../reference_manual/dr_minw_debugger.rst:13
msgid "Debug"
msgstr "Depurar"

#: ../../reference_manual/dr_minw_debugger.rst:18
msgid "Dr. MinW Debugger"
msgstr "Depurador Dr. MinW"

#: ../../reference_manual/dr_minw_debugger.rst:22
msgid ""
"The information on this page applies only to the Windows release of Krita "
"3.1 Beta 3 (3.0.92) and later."
msgstr ""
"La informació d'aquesta pàgina només s'aplica als llançaments per a Windows "
"del Krita 3.1 Beta 3 (3.0.92) i posteriors."

#: ../../reference_manual/dr_minw_debugger.rst:26
msgid "Getting a Backtrace"
msgstr "Obtenir una traça inversa"

#: ../../reference_manual/dr_minw_debugger.rst:28
msgid ""
"There are some additions to Krita which makes getting a backtrace much "
"easier on Windows."
msgstr ""
"Hi ha algunes addicions al Krita que faciliten molt el procés de traça "
"inversa sobre el Windows."

#: ../../reference_manual/dr_minw_debugger.rst:32
msgid ""
"When there is a crash, Krita might appear to be unresponsive for a short "
"time, ranging from a few seconds to a few minutes, before the crash dialog "
"appears."
msgstr ""
"Quan es produeix un bloqueig, pot semblar que el Krita no respon durant un "
"breu període de temps, des d'uns pocs segons fins a uns quants minuts, abans "
"que aparegui el diàleg de bloqueig."

#: ../../reference_manual/dr_minw_debugger.rst:36
msgid "An example of the crash dialog."
msgstr "Un exemple del diàleg de bloqueig."

#: ../../reference_manual/dr_minw_debugger.rst:38
msgid ""
"If Krita keeps on being unresponsive for more than a few minutes, it might "
"actually be locked up, which may not give a backtrace. In that situation, "
"you have to close Krita manually. Continue to follow the following "
"instructions to check whether it was a crash or not."
msgstr ""
"Si el Krita segueix sense respondre durant més d'uns quants minuts, podria "
"ser que estigui bloquejat, el qual no podrà generar una traça inversa. En "
"aquesta situació, haureu de tancar el Krita manualment. Continueu seguint "
"les següents instruccions per a verificar si s'ha o no bloquejat."

# skip-rule: t-acc_obe
#: ../../reference_manual/dr_minw_debugger.rst:40
msgid ""
"Open Windows Explorer and type ``%LocalAppData%`` (without quotes) on the "
"address bar and press the :kbd:`Enter` key."
msgstr ""
"Obriu l'Explorador de Windows i escriviu ``%LocalAppData%`` (sense cometes) "
"a la barra d'adreces i premeu la tecla :kbd:`Retorn`."

# skip-rule: t-acc_obe
#: ../../reference_manual/dr_minw_debugger.rst:44
msgid ""
"Find the file ``kritacrash.log`` (it might appear as simply ``kritacrash`` "
"depending on your settings.)"
msgstr ""
"Trobeu el fitxer ``kritacrash.log`` (pot aparèixer simplement com a "
"``kritacrash``, segons els vostres ajustaments)."

#: ../../reference_manual/dr_minw_debugger.rst:45
msgid ""
"Open the file with Notepad and scroll to the bottom, then scroll up to the "
"first occurrence of “Error occurred on <time>” or the dashes."
msgstr ""
"Obriu el fitxer amb el Bloc de notes i desplaceu-vos fins a la part "
"inferior, després aneu a la primera ocurrència de «Error occurred on "
"<temps>» o dels guions."

#: ../../reference_manual/dr_minw_debugger.rst:49
msgid "Start of backtrace."
msgstr "Inici de la traça inversa."

#: ../../reference_manual/dr_minw_debugger.rst:51
msgid "Check the time and make sure it matches the time of the crash."
msgstr ""
"Comproveu l'hora i assegureu-vos que coincideixi amb l'hora del bloqueig."

#: ../../reference_manual/dr_minw_debugger.rst:55
msgid "End of backtrace."
msgstr "Final de la traça inversa."

#: ../../reference_manual/dr_minw_debugger.rst:57
msgid ""
"The text starting from this line to the end of the file is the most recent "
"backtrace."
msgstr ""
"El text que comença des d'aquesta línia fins al final del fitxer és la traça "
"inversa més recent."

# skip-rule: t-acc_obe
#: ../../reference_manual/dr_minw_debugger.rst:59
msgid ""
"If ``kritacrash.log`` does not exist, or a backtrace with a matching time "
"does not exist, then you don’t have a backtrace. This means Krita was very "
"likely locked up, and a crash didn’t actually happen. In this case, make a "
"bug report too."
msgstr ""
"Si el ``kritacrash.log`` no existeix, o no hi ha un temps de seguiment que "
"coincideixi, llavors no tindreu cap traça inversa. Això voldrà dir que el "
"Krita probablement estava molt bloquejat, i que en realitat no s'ha produït "
"cap bloqueig. En aquest cas, feu també un informe d'error."

#: ../../reference_manual/dr_minw_debugger.rst:60
msgid ""
"If the backtrace looks truncated, or there is nothing after the time, it "
"means there was a crash and the crash handler was creating the stack trace "
"before being closed manually. In this case, try to re-trigger the crash and "
"wait longer until the crash dialog appears."
msgstr ""
"Si la traça inversa es veu truncada, o no hi ha res després del temps, això "
"voldrà dir que hi va haver un bloqueig i que el manipulador de bloquejos "
"estava creant la traça de la pila abans de tancar manualment. En aquest cas, "
"intenteu tornar a activar el bloqueig i espereu més fins que aparegui el "
"diàleg de bloqueig."

#: ../../reference_manual/dr_minw_debugger.rst:64
msgid ""
"Starting from Krita 3.1 Beta 3 (3.0.92), the external DrMingw JIT debugger "
"is not needed for getting the backtrace."
msgstr ""
"A partir del Krita 3.1 Beta 3 (3.0.92), el depurador extern DrMingw del JIT "
"ja no és necessari per obtenir la traça inversa."

#: ../../reference_manual/dr_minw_debugger.rst:67
msgid "Using the Debug Package"
msgstr "Ús del paquet de depuració"

#: ../../reference_manual/dr_minw_debugger.rst:69
msgid ""
"Starting from 3.1 Beta 3, the debug package contains only the debug symbols "
"separated from the executables, so you have to download the portable package "
"separately too (though usually you already have it in the first place.)"
msgstr ""
"A partir de la versió 3.1 Beta 3, el paquet de depuració només conté els "
"símbols de depuració separats dels executables, per la qual cosa també "
"haureu de descarregar el paquet portable per separat (encara que normalment "
"ja el teníeu)."

#: ../../reference_manual/dr_minw_debugger.rst:71
msgid ""
"Links to the debug packages should be available on the release announcement "
"news item on https://krita.org/, along with the release packages. You can "
"find debug packages for any release either in https://download.kde.org/"
"stable/krita for stable releases or in https://download.kde.org/unstable/"
"krita for unstable releases. Portable zip and debug zip are found next to "
"each other."
msgstr ""
"Els enllaços als paquets de depuració haurien d'estar disponibles a la "
"notícia de l'anunci de la versió a https://krita.org/, juntament amb els "
"paquets de la versió. Trobareu els paquets de depuració per a qualsevol "
"versió a https://download.kde.org/stable/krita per a les versions estables o "
"a https://download.kde.org/unstable/krita per a les versions inestables. El "
"ZIP portable i el ZIP de depuració es troben un al costat de l'altre."

#: ../../reference_manual/dr_minw_debugger.rst:72
msgid ""
"Make sure you’ve downloaded the same version of debug package for the "
"portable package you intend to debug / get a better (sort of) backtrace."
msgstr ""
"Assegureu-vos que heu descarregat la mateixa versió del paquet de depuració "
"per al paquet portable en el qual intenteu depurar/obtenir una traça inversa "
"millor (més o menys)."

# skip-rule: t-acc_obe
#: ../../reference_manual/dr_minw_debugger.rst:73
msgid ""
"Extract the files inside the Krita install directory, where the sub-"
"directories `bin`, `lib` and `share` is located, like in the figures below:"
msgstr ""
"Extraieu els fitxers dins del directori d'instal·lació del Krita, on es "
"troben els subdirectoris `bin`, `lib` i `share`, com en la següent captura "
"de pantalla:"

# skip-rule: t-acc_obe
#: ../../reference_manual/dr_minw_debugger.rst:79
msgid ""
"After extracting the files, check the ``bin`` dir and make sure you see the "
"``.debug`` dir inside. If you don't see it, you probably extracted to the "
"wrong place."
msgstr ""
"Després d'extreure els fitxers, comproveu el directori ``bin`` i assegureu-"
"vos que hi veieu el directori ``.debug`` a dins. Si no el veieu, "
"probablement ho heu extret al lloc equivocat."
