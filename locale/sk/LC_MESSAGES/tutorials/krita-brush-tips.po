# translation of docs_krita_org_tutorials___krita-brush-tips.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_tutorials___krita-brush-tips\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-22 09:19+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../tutorials/krita-brush-tips.rst:1
#: ../../tutorials/krita-brush-tips.rst:13
msgid ""
"Krita Brush-tips is an archive of brush-modification tutorials done by the "
"krita-foundation.tumblr.com account based on user requests."
msgstr ""

#: ../../tutorials/krita-brush-tips.rst:15
msgid "Topics:"
msgstr "Témy:"
