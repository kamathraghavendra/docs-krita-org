# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:10+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: högerknapp"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:1
msgid ""
"Split Alpha: how to work with color and alpha channels of the layer "
"separately"
msgstr "Dela alfa: hur man arbetar med färger och alfakanaler i lagret separat"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:11
msgid "Layers"
msgstr "Lager"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:11
msgid "Transparency"
msgstr "Genomskinlighet"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:11
msgid "Alpha channel"
msgstr "Alfakanal"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:11
msgid "Game"
msgstr "Spel"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:16
msgid "Split Alpha"
msgstr "Dela alfa"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:18
msgid ""
"Sometimes especially in the field of game development, artists need to work "
"with the alpha channel of the texture separately. To assist such workflow, "
"Krita has a special functionality called :menuselection:`Split Alpha`. It "
"allows splitting alpha channel of a paint layer into a separate :ref:"
"`transparency_masks`. The artist can work on the transparency mask in an "
"isolated environment and merge it back when he has finished working."
msgstr ""
"Ibland behöver konstnärer arbeta med alfakanalen i en struktur separat, "
"särskilt på spelutvecklingsområdet. För att hjälpa till med ett sådant "
"arbetsflöde har Krita en särskild funktion som kallas :menuselection:`Dela "
"alfa`. Det gör det möjligt att dela alfakanalen på ett målarlager till "
"separata :ref:`transparency_masks`. Konstnären kan arbeta med "
"genomskinlighetsmasken i en isolerad omgivning och sammanfoga den tillbaka "
"när arbetet är färdigt."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:21
msgid "How to work with alpha channel of the layer"
msgstr "Hur man arbetar med lagrets alfakanal"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:23
msgid "|mouseright| the paint layer in the layers docker."
msgstr "Högerklicka på målarlagret i lagerpanelen."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:24
#: ../../reference_manual/layers_and_masks/split_alpha.rst:35
msgid "Choose :menuselection:`Split Alpha --> Alpha into Mask`."
msgstr "Välj :menuselection:`Dela alfa --> Alfa till mask`."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:25
msgid ""
"Use your preferred paint tool to paint on the Transparency Mask. Black "
"paints transparency (see-through), white paints opacity (visible). Gray "
"values paint semi-transparency."
msgstr ""
"Använd föredraget målarverktyg för att måla på genomskinlighetsmasken. Svart "
"målar genomskinlighet (transparent), vitt målar ogenomskinlighet (synligt). "
"Gråa värden målar halvgenomskinligt."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:26
msgid ""
"If you would like to isolate alpha channel, enter Isolated Mode by |"
"mouseright| + :menuselection:`Isolate Layer` (or the :kbd:`Alt +` |"
"mouseleft| shortcut)."
msgstr ""
"Om man vill isolera en alfakanal, gå till isolationsläge med högerklick + :"
"menuselection:`Isolera lager` (eller genvägen :kbd:`Alt` + vänsterklick)."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:27
msgid ""
"When finished editing the Transparency Mask, |mouseright| on it and select :"
"menuselection:`Split Alpha --> Write as Alpha`."
msgstr ""
"När man är klar med redigering av genomskinlighetsmasken, högerklicka på den "
"och välj :menuselection:`Dela alfa --> Skriv som alfa`."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:30
msgid ""
"How to save a PNG texture and keep color values in fully transparent areas"
msgstr ""
"Hur man sparar PNG-strukturer och behåll färgvärden på helt genomskinliga "
"områden"

#: ../../reference_manual/layers_and_masks/split_alpha.rst:32
msgid ""
"Normally, when saving an image to a file, all fully transparent areas of the "
"image are filled with black color. It happens because when composing the "
"layers of the image, Krita drop color data of fully transparent pixels for "
"efficiency reason. To avoid this of color data loss you can either avoid "
"compositing of the image i.e. limit image to only one layer without any "
"masks or effects, or use the following method:"
msgstr ""
"Normalt fylls alla helt genomskinliga områden på en bild med svart färg när "
"bilden sparas i en fil. Det sker eftersom när Krita sätter samman bildens "
"lager tas färgdata för helt genomskinliga bildpunkter bort av "
"effektivitetsskäl. För att undvika sådan färgförlust kan man antingen "
"undvika att sammansätta bilden, dvs. begränsa bilden till bara ett lager "
"utan några masker eller effekter, eller använda följande metod: "

#: ../../reference_manual/layers_and_masks/split_alpha.rst:34
msgid "|mouseright| the layer in the layers docker."
msgstr "Högerklicka på lagret i lagerpanelen."

#: ../../reference_manual/layers_and_masks/split_alpha.rst:36
msgid ""
"|mouseright| on the created mask and select :menuselection:`Split Alpha --> "
"Save Merged...`"
msgstr ""
"Högerklicka på den skapade masken och välj :menuselection:`Dela alfa --> "
"Spara sammanfogad...`"
