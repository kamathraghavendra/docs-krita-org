# Translation of docs_krita_org_reference_manual___brushes___brush_engines___filter_brush_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___filter_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 13:46+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:1
msgid "The Filter Brush Engine manual page."
msgstr "Розділ підручника щодо рушія пензлів фільтрування."

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:17
msgid "Filter Brush Engine"
msgstr "Рушій пензлів фільтрування"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:12
msgid "Filters"
msgstr "Фільтри"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:20
msgid ".. image:: images/icons/filterbrush.svg"
msgstr ".. image:: images/icons/filterbrush.svg"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:21
msgid ""
"Where in other programs you have a 'dodge tool', 'blur tool' and 'sharpen "
"tool', Krita has a special brush engine for this: The Filter Brush engine. "
"On top of that, due to Krita's great integration of the filters, a huge "
"amount of filters you'd never thought you wanted to use for a drawing are "
"possible in brush form too!"
msgstr ""
"Де у інших програмах ви маєте справу з «інструментом приховування», "
"«інструментом розмивання» та «інструментом збільшення різкості», у Krita "
"передбачено спеціальний рушій пензлів — рушій пензлів фільтрування. "
"Надбудовою до цього рушія, оскільки у Krita має чудові можливості із "
"інтеграції фільтрів, є широкий діапазон фільтрів на будь-який смак, якими "
"можна скористатися для малювання пензлями будь-яких форм!"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:24
msgid "Options"
msgstr "Параметри"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:26
msgid "The filter brush has of course some basic brush-system parameters:"
msgstr ""
"У пензля фільтрування, звичайно ж, є декілька базових параметрів системи "
"пензлів:"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:28
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:29
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:30
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:31
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:32
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/filter_brush_engine.rst:33
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"
