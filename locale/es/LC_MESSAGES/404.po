# Spanish translations for docs_krita_org_404.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Sofia Priego <spriego@darksylvania.net>, %Y.
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_404\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-30 20:28+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../404.rst:None
#, fuzzy
#| msgid ""
#| ".. image:: images/en/color_category/Kiki_cLUTprofiles.png\n"
#| "   :alt: Image of Kiki looking confused through books."
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""
".. image:: images/en/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Imagen de Kiki, que parece confundida entre libros."

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr "Archivo no encontrado (404)"

#: ../../404.rst:10
msgid "This page does not exist."
msgstr "Esta página no existe."

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr "Se puede deber a lo siguiente:"

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""
"Hemos movido el manual de MediaWiki a Sphinx, lo que ha conllevado gran "
"cantidad de reorganización."

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr "La página se ha quedado obsoleta."

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr "O un simple error de escritura en el URL."

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr ""
"En cualquier caso, puede encontrar la página que está buscando mediante la "
"búsqueda que hay en el panel de navegación de la izquierda."
