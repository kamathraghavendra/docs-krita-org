# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 10:03+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en image Laplaciano variable depth images Emboss\n"
"X-POFile-SpellExtra: Laplaciana filters\n"

#: ../../reference_manual/filters/emboss.rst:1
msgid "Overview of the emboss filters."
msgstr "Introdução aos filtros de relevo."

#: ../../reference_manual/filters/emboss.rst:10
#: ../../reference_manual/filters/emboss.rst:15
msgid "Emboss"
msgstr "Elevar"

#: ../../reference_manual/filters/emboss.rst:10
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/filters/emboss.rst:17
msgid ""
"Filters that are named by the traditional embossing technique. This filter "
"generates highlight and shadows to create an effect which makes the image "
"look like embossed. Emboss filters are usually used in the creation of "
"interesting GUI elements, and mostly used in combination with filter-layers "
"and masks."
msgstr ""
"Os filtros que derivam o seu nome da tradicional técnica de elevação em "
"relevo. Este filtro gera alguma luminosidade e sombras para criar um efeito "
"que faz com que a imagem pareça gravada em relevo. Os filtros de relevo ou "
"elevação são normalmente usados na criação de elementos gráficos "
"interessantes e são usadas maioritariamente em conjunto com as camadas de "
"filtragem e máscaras."

#: ../../reference_manual/filters/emboss.rst:20
msgid "Emboss Horizontal Only"
msgstr "Elevar na Horizontal Apenas"

#: ../../reference_manual/filters/emboss.rst:22
msgid "Only embosses horizontal lines."
msgstr "Só eleva as linhas horizontais."

#: ../../reference_manual/filters/emboss.rst:25
msgid "Emboss in all Directions"
msgstr "Elevar em Todas as Direcções"

#: ../../reference_manual/filters/emboss.rst:27
msgid "Embosses in all possible directions."
msgstr "Eleva em todas as direcções possíveis."

#: ../../reference_manual/filters/emboss.rst:30
msgid "Emboss (Laplacian)"
msgstr "Elevar (Laplaciano)"

#: ../../reference_manual/filters/emboss.rst:32
msgid "Uses the laplacian algorithm to perform embossing."
msgstr "Usa o algoritmo Laplaciano para efectuar a elevação."

#: ../../reference_manual/filters/emboss.rst:35
msgid "Emboss Vertical Only"
msgstr "Elevar na Vertical Apenas"

#: ../../reference_manual/filters/emboss.rst:37
msgid "Only embosses vertical lines."
msgstr "Só eleva as linhas verticais."

#: ../../reference_manual/filters/emboss.rst:40
msgid "Emboss with Variable depth"
msgstr "Elevar com Profundidade Variável"

#: ../../reference_manual/filters/emboss.rst:42
msgid ""
"Embosses with a depth that can be set through the dialog box shown below."
msgstr ""
"Cria uma elevação com uma profundidade que pode ser definida através da "
"janela apresentada abaixo."

#: ../../reference_manual/filters/emboss.rst:45
msgid ".. image:: images/filters/Emboss-variable-depth.png"
msgstr ".. image:: images/filters/Emboss-variable-depth.png"

#: ../../reference_manual/filters/emboss.rst:47
msgid "Emboss Horizontal and Vertical"
msgstr "Elevação na Horizontal e na Vertical"

#: ../../reference_manual/filters/emboss.rst:49
msgid "Only embosses horizontal and vertical lines."
msgstr "Só eleva as linhas horizontais e verticais."
