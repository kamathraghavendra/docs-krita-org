# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-03 15:22+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: icons Krita image toolgradientedit images alt ref\n"
"X-POFile-SpellExtra: gradientedittool shapeselectiontool\n"

#: ../../<rst_epilog>:18
msgid ""
".. image:: images/icons/gradient_edit_tool.svg\n"
"   :alt: toolgradientedit"
msgstr ""
".. image:: images/icons/gradient_edit_tool.svg\n"
"   :alt: ferramenta de edição de gradientes"

#: ../../reference_manual/tools/gradient_edit.rst:1
msgid "Krita's vector gradient editing tool reference."
msgstr ""
"A referência da ferramenta de edição de gradientes vectoriais do Krita."

#: ../../reference_manual/tools/gradient_edit.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/gradient_edit.rst:11
msgid "Gradient"
msgstr "Gradiente"

#: ../../reference_manual/tools/gradient_edit.rst:16
msgid "Gradient Editing Tool"
msgstr "Ferramenta de Edição de Gradientes"

#: ../../reference_manual/tools/gradient_edit.rst:18
msgid "|toolgradientedit|"
msgstr "|toolgradientedit|"

#: ../../reference_manual/tools/gradient_edit.rst:22
msgid ""
"This tool has been removed in Krita 4.0, and its functionality has been "
"folded into the :ref:`shape_selection_tool`."
msgstr ""
"Esta ferramenta foi removida no Krita 4.0 e a sua funcionalidade foi "
"incorporada na :ref:`shape_selection_tool`."

#: ../../reference_manual/tools/gradient_edit.rst:24
msgid ""
"This tool allows you to edit the gradient on canvas, but it only works for "
"vector layers. If you have a vector shape selected, and draw a line over the "
"canvas, you will be able to see the nodes, and the stops in the gradient. "
"Move around the nodes to move the gradient itself. Select the stops to "
"change their color in the tool options docker, or to move their position in "
"the on canvas gradient. You can select preset gradient in the tool docker to "
"change the active shape's gradient to use those stops."
msgstr ""
"Esta ferramenta permite-lhe editar o gradiente na área de desenho, mas só "
"funciona com camadas vectoriais. Se tiver uma forma vectorial seleccionada e "
"desenhar uma linha sobre a área de desenho, poderá ver os nós e as paragens "
"no gradiente. Mova os nós para modificar o gradiente em si. Seleccione as "
"paragens para modificar a sua cor na área de opções da ferramenta ou para "
"mover a sua posição no gradiente directamente na área de desenho. Poderá "
"seleccionar um gradiente predefinido na área da ferramenta para mudar o "
"gradiente da forma activa de forma a usar essas paragens."
