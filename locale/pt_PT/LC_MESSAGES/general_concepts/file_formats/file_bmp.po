# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:51+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: bitmap filepng Bitmap ref\n"

#: ../../general_concepts/file_formats/file_bmp.rst:1
msgid "The Bitmap file format."
msgstr "O formato de ficheiros Bitmap."

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "*.bmp"
msgstr "*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "BMP"
msgstr "BMP"

#: ../../general_concepts/file_formats/file_bmp.rst:10
msgid "Bitmap Fileformat"
msgstr "Formato de Ficheiros Bitmap"

#: ../../general_concepts/file_formats/file_bmp.rst:15
msgid "\\*.bmp"
msgstr "\\*.bmp"

#: ../../general_concepts/file_formats/file_bmp.rst:17
msgid ""
"``.bmp``, or Bitmap, is the simplest raster file format out there, and, "
"being patent-free, most programs can open and save bitmap files."
msgstr ""
"O ``.bmp``, ou Bitmap, é o formato de ficheiros rasterizados mais simples "
"por aí e, sendo livre de patentes, a maioria dos programas consegue abrir e "
"gravar estes ficheiros de imagens."

#: ../../general_concepts/file_formats/file_bmp.rst:19
msgid ""
"However, most programs don't compress bitmap files, leading to BMP having a "
"reputation for being very heavy. If you need a lossless file format, we "
"actually recommend :ref:`file_png`."
msgstr ""
"Contudo, a maioria dos programas não comprime os ficheiros 'bitmap', o que "
"leva a que um ficheiro BMP tem a reputação de ser demasiado grande. Se "
"precisar de um formato de ficheiros sem perdas, nós recomendamos neste "
"momento o :ref:`file_png`."
